package com.thg.accelerator;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
  /**
   * Rigorous Test :-)
   */


  @Test
  public void shouldAnswerWithTrue() {
    assertTrue(true);
  }

  @Test
  public void testtest() {
    assertTrue(1 + 1 == 2);
  }

  @Test
  public void test2() {
    Method m1 = new Method(1);
    int a = m1.addnum(9, 9);
    Assert.assertEquals(18, a);
  }

  @Test
  public void test3() {
    Method m1 = new Method(2);
    int a = m1.addnum(1, 9);
    Assert.assertEquals(10, a);
  }


}
